/**
 * react-timer
**/

var SecondsTohhmmss = function(totalSeconds) {
  var hours   = Math.floor(totalSeconds / 3600);
  var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
  var seconds = totalSeconds - (hours * 3600) - (minutes * 60);

  // round seconds
  seconds = Math.round(seconds * 100) / 100

  var result = (hours < 10 ? "0" + hours : hours);
      result += ":" + (minutes < 10 ? "0" + minutes : minutes);
      result += ":" + (seconds  < 10 ? "0" + seconds : seconds);
  return result;
}


/**
 * Timer module
 * A simple timer component.
**/

var Timer = React.createClass({displayName: "Timer",
  getInitialState: function(){
     return {
       clock: 0,
       time: '',
       prefix: this.props.options.prefix,
     }
  },
  /**
   * Pause the timer.
  **/
  pause: function() {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  },
  /** 
   * Play the timer.
  **/
  play: function() {
    if (!this.interval) {
      this.offset   = Date.now();
      this.interval = setInterval(this.update, this.props.options.delay); // 100 is delay
    }
  },
  /** 
   * Reset the timer.
  **/
  reset: function() {
    var clockReset = 0;
    this.setState({clock: clockReset });
    var time = SecondsTohhmmss(clockReset / 1000);
    this.setState({time: time });    
  },
  /** 
   * Increment the timer.
  **/
  update: function() {
    var clock = this.state.clock;
    clock += this.calculateOffset();
    this.setState({clock: clock });
    var time = SecondsTohhmmss(clock / 1000);
    this.setState({time: time });    
  },
  /** 
   * Calculate the offset time.
  **/
  calculateOffset: function() {
    var now = Date.now(),
        o   = now - this.offset;    
    this.offset = now;
    return o;
  },
  componentDidMount: function() {
    this.play();
  },
  componentWillUnmount: function() {
    this.pause();
  },  
  render: function(){
    return (

      React.createElement("div", {className: "react-timer"}, 

      React.createElement("h3", {className: "seconds"}, " ", this.state.time, " ", this.state.prefix), 

       React.createElement("br", null), 

       React.createElement("button", {onClick: this.reset, className: "pure-button button-secondary"}, "reset"), 
       React.createElement("button", {onClick: this.play, className: "pure-button button-secondary"}, "play"), 
       React.createElement("button", {onClick: this.pause, className: "pure-button button-secondary"}, "pause")
      )
    );
  }
});

/**
 * react-search
**/

var SearchItemInArray = function(items, input) {

    var reg = new RegExp(input.split('').join('\\w*').replace(/\W/, ""), 'i');
    
    return items.filter(function(item) {
      if (item.match(reg)) {
        return item;
      }
    });
}

/**
 * Search module
 * A simple search box component.
**/

var Search = React.createClass({displayName: "Search",
  getInitialState: function(){
     return {
       items:  this.props.items,
       matchingItems: [],
       searchValue: ''
     }
  },
  componentDidMount: function() {
  },
  componentWillUnmount: function() {
  },  
  /** 
   * Input box text has changed, trigger update of the autocomplete box.
  **/
  changeInput: function () {
    var autocomplete = this.refs.autocomplete.getDOMNode();
    autocomplete.className = "pure-menu pure-menu-open";
    var searchValue = this.refs.searchInput.getDOMNode().value;
    var result = SearchItemInArray(this.state.items, searchValue);
    this.setState({matchingItems: result});
  },
  selectAutoComplete: function (e) {
    var autocomplete = this.refs.autocomplete.getDOMNode();
    autocomplete.className = "pure-menu pure-menu-hidden";
    var result = e.target.innerHTML;
    this.refs.searchInput.getDOMNode().value = result;
  },
  render: function(){

    var items = this.state.matchingItems.map(function (item) {
      return (
        React.createElement("li", null, 
          React.createElement("a", {onClick: this.selectAutoComplete}, 
            item
          )
        )
      );
    }.bind(this));

    return (
      React.createElement("div", {className: "react-search"}, 
       React.createElement("input", {type: "text", className: "input-text", ref: "searchInput", onKeyUp: this.changeInput}), 

        React.createElement("div", {className: "pure-menu pure-menu-hidden", ref: "autocomplete"}, 
          React.createElement("ul", null, 
          items
          )
        )

      )
    );
  }
});

/**
 * react-count-down
**/

var DateBetween = function(startDate, endDate) {
  var second = 1000;
  var minute = second * 60;
  var hour = minute * 60;
  var day = hour * 24;
  var distance = endDate - startDate;
  
  if (distance < 0) {
    return "count down date expired";
  }

  var days = Math.floor(distance / day);
  var hours = Math.floor((distance % day) / hour);
  var minutes = Math.floor((distance % hour) / minute);
  var seconds = Math.floor((distance % minute) / second);

  var between = days + ' days ';
  between += hours + ' hours ';
  between += minutes + ' minutes ';
  between += seconds + ' seconds';

  return between;
}


/**
 * Count down module
 * A simple count down component.
**/

var CountDown = React.createClass({displayName: "CountDown",
  getInitialState: function(){
     return {
       endDate: this.props.options.endDate,
       prefix: this.props.options.prefix
     }
  },
  /** 
   * Tick the counter down.
  **/
  tick: function() {
    var startDate = new Date();
    var endDate = new Date(this.state.endDate);
    var remaining = DateBetween(startDate, endDate);
    this.setState({remaining: remaining });
  },
  componentDidMount: function() {
    this.tick();
    this.interval = setInterval(this.tick, 1000);
  },
  componentWillUnmount: function() {
    clearInterval(this.interval);
  },  
  render: function(){
    return (
      React.createElement("div", {className: "react-count-down"}, 
       React.createElement("span", {className: "date"}, " ", this.state.remaining), 
       React.createElement("span", {className: "prefix"}, " ", this.state.prefix)
      )
    );
  }
});

/**
 * react-sortable-list
**/

/**
 * Sortable List module
 * A sortable list component using html5 drag and drop api.
**/

var placeholder = document.createElement("li");
placeholder.className = "placeholder";

var SortableList = React.createClass({displayName: "SortableList",
  getInitialState: function() {
    return {data: this.props.data};
  },
  /** 
   * On drag start, set data.
  **/
  dragStart: function(e) {
    this.dragged = e.currentTarget;
    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData("text/html", e.currentTarget);
  },
  /** 
   * On drag end, update the data state.
  **/
  dragEnd: function(e) {
    this.dragged.style.display = "block";
    this.dragged.parentNode.removeChild(placeholder);
    var data = this.state.data;
    var from = Number(this.dragged.dataset.id);
    var to = Number(this.over.dataset.id);
    if(from < to) to--;
    if(this.nodePlacement == "after") to++;
    data.splice(to, 0, data.splice(from, 1)[0]);
    this.setState({data: data});
  },
  /** 
   * On drag over, update items.
  **/
  dragOver: function(e) {
    e.preventDefault();
    this.dragged.style.display = "none";
    if(e.target.className == "placeholder") return;
    this.over = e.target;
    var relY = e.clientY - this.over.offsetTop;
    var height = this.over.offsetHeight / 2;
    var parent = e.target.parentNode;
    
    if(relY > height) {
      this.nodePlacement = "after";
      parent.insertBefore(placeholder, e.target.nextElementSibling);
    }
    else if(relY < height) {
      this.nodePlacement = "before"
      parent.insertBefore(placeholder, e.target);
    }
  },
  render: function() {
    var listItems = this.state.data.map((function(item, i) {
      return (

          React.createElement("li", {className: "react-sortable", "data-id": i, 
              key: i, 
              draggable: "true", 
              onDragEnd: this.dragEnd, 
              onDragStart: this.dragStart}, 
            item
          )
      );
    }).bind(this));

    return React.createElement("ul", {onDragOver: this.dragOver}, listItems)
  }
});