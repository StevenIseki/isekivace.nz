var layout   = document.getElementById('layout'),
    menu     = document.getElementById('menu'),
    menuLink = document.getElementById('menuLink');

function toggleClass(element, className) {
    var classes = element.className.split(/\s+/),
        length = classes.length,
        i = 0;

    for(; i < length; i++) {
      if (classes[i] === className) {
        classes.splice(i, 1);
        break;
      }
    }
    // The className is not found
    if (length === classes.length) {
        classes.push(className);
    }

    element.className = classes.join(' ');
}


menuLink.onclick = function (e) {
    var active = 'active';

    e.preventDefault();
    toggleClass(layout, active);
    toggleClass(menu, active);
    toggleClass(menuLink, active);
};    

// init react timer
var OPTIONS = { prefix: 'seconds elapsed!', delay: 100}

React.render(
    React.createElement(
        Timer, 
        {options: OPTIONS}
    ),
    document.getElementById("react-timer")
);

// init react search
var ITEMS = ['ruby', 'javascript', 'lua', 'go', 'c++', 'julia', 'java', 'c', 'scala','haskell']

React.render(
    React.createElement(
        Search, 
        {items: ITEMS}
    ),
    document.getElementById("react-search")
);

// init react count down
var CD_OPTIONS = { endDate: '06/03/2015 10:12 AM', prefix: 'until my birthday!' }

React.render(
    React.createElement(
        CountDown, 
        {options: CD_OPTIONS}
    ),
    document.getElementById("react-count-down")
);

// init react sortable list
var colors = ["Red","Blue","Yellow","White"];

React.render(
    React.createElement(
        SortableList, 
        {data: colors}
    ),
    document.getElementById("react-sortable-list")
);
